void main(List<String> arguments) {
  //Creating an Array List
  var arrAppOfTheYear = [
    "PhraZ App 2012",
    "SnapScan App 2013",
    "Live Inspect App 2014",
    "WumDrop App 2015",
    "Domestly App 2016",
    "Standard Bank Shyft App 2017",
    "Khula App 2018",
    "Naked Insurance App 2019",
    "EasyEquities App 2020",
    "Ambani Africa App 2021"
  ];

  //Sorting List
  arrAppOfTheYear.sort();

  //Printing Sorted List
  for (var sortList in arrAppOfTheYear) {
    print(sortList + "\n");
  }

  //Assigning a variable to position 8
  String app2017 = arrAppOfTheYear[8];

  //Printing the winning app of 2017
  print("Winning App for 2017 = $app2017" + ("\n"));

  //Assigning a variable to position 3
  String app2018 = arrAppOfTheYear[3];

  //Printing the winning app of 2018
  print("Winning App for 2018 = $app2018" + ("\n"));

  //Assigning a variable to Array Lenght
  int total = arrAppOfTheYear.length;

  //Print total number of apps from the array.
  print("Total number of Apps = $total");
}
