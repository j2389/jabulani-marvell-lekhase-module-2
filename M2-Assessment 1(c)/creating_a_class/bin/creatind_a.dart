class MTNBusinessApp {
  //Declaring Variables and initializing
  String nameApp = " ";
  String category = " ";
  String developerName = " ";
  int mtnAppYear = 0;

  // Constructor
  MTNBusinessApp(
      String nameApp, String category, String developerName, int mtnAppYear) {
    this.nameApp = nameApp;
    this.category = category;
    this.developerName = developerName;
    this.mtnAppYear = mtnAppYear;
  }

  // Creating the getter method
  // to get input from Field/Property
  String get getNameApp {
    return nameApp;
  }

  String get getCategory {
    return category;
  }

  String get getDeveloperName {
    return developerName;
  }

  int get getMtnAppYear {
    return mtnAppYear;
  }

  // Creating the setter method
  // to set the input in Field/Property
  set setNameApp(String nameApp) {
    this.nameApp = nameApp;
  }

  set setCategory(String category) {
    this.category = category;
  }

  set setDeveloperName(String developerName) {
    this.developerName = developerName;
  }

  set setMtnAppYear(int mtnAppYear) {
    this.mtnAppYear = mtnAppYear;
  }

  //Function method for capital letters
  void capitalLetters() {
    print("Application Name in Capital Letters :" + nameApp.toUpperCase());
  }
}

void main(List<String> arguments) {
  // Ceating Object for MTN Class
  var objMTNBussinessApp = MTNBusinessApp(
      "Ambani Africa App", "Best Gaming Solution", "Ambani", 2021);

  //Printing variables from Class
  print("Application Name : " + objMTNBussinessApp.nameApp);
  print("Category Name : " + objMTNBussinessApp.category);
  print("Developer Name : " + objMTNBussinessApp.developerName);
  print("Winning Year : " + objMTNBussinessApp.mtnAppYear.toString());

  //Printing Funtion in Capital Letters
  objMTNBussinessApp.capitalLetters();
}
